import pickle
import statistics
from gensim.models import KeyedVectors
import numpy as np
from keras.utils import to_categorical
from keras.preprocessing.sequence import pad_sequences


def find_word_embedding(chinese_model, word):
    '''
    Finds the word embedding from pretrained chinese model.
    If word not into vocabulary returns a vectors of ones.
    :param chinese_model: pretrained word embeddind
    :param word: unigram or bigram
    :return: word embedding
    '''
    if word in chinese_model.vocab.keys():
        return chinese_model.wv[word]
    else:
        return np.ones(chinese_model.vector_size)


def create_input_label_file(paths):
    '''
    Create the input file without space and the BIES labels files
    :param paths: a list of a files or a single file
    :return: output_lines : a list of list of characters,
             labels : a list of list of BIES labels for each char
    '''
    lines = []
    # Open dataset files
    if type(paths) == list:
        for path in paths:
            with open(path, encoding="utf-8") as f:
                lines += f.read().splitlines()
    else:
        with open(paths, encoding="utf-8") as f:
            lines = f.read().splitlines()

    labels, output_lines = [], []
    # Iterate over lines
    for (line, j) in zip(lines, range(0, len(lines))):
        output_line = []
        output_line_labels = []
        for i in range(0, len(line)):
            # If char is a space, continue
            if line[i] == " " or line[i] == '\u3000':  # \u3000 ideographic space character
                continue
            # Case 1 : we are at the end of the line
            if i == len(line)-1:
                if i > 0:  # We are sure that the line is not a single char
                    if line[i-1] != ' ' or line[i-1] != '\u3000':
                        output_line_labels.append('S')
                    else:
                        output_line_labels.append('E')
                else:
                    # Line is a single char and we are at the end
                    output_line_labels.append('S')
                output_line.append(line[i])
                break
            # Case 2: next char is a space
            if line[i+1] == " " or line[i+1] == '\u3000':
                # Check if we are at the beginning of the file
                if i == 0:
                    output_line_labels.append('S')
                elif line[i-1] == " " or line[i-1] == '\u3000':
                    # Or at the end of some single word
                    output_line_labels.append('S')
                # Or at the end of some word
                else:
                    output_line_labels.append('E')
            else:
                # Case 3: next char is a chinese char
                if i == 0:
                    # We are at beggining of the line
                    output_line_labels.append('B')
                elif i - 1 >= 0:
                    # We are at the beggining of some word
                    if line[i-1] == " " or line[i-1] == '\u3000':
                        output_line_labels.append('B')
                    else:
                        # We are at the middle of some word
                        output_line_labels.append('I')
            output_line.append(line[i])
        output_lines.append("".join(output_line))
        labels.append("".join(output_line_labels))

    return output_lines, labels


def create_unigrams_and_labels(lines, labels, N_CLASS, MAX_LENGTH):
    '''
    Create the indexed unigram object with OHE labels
    :return: unigram object, labels
    '''
    encoding_labels = {'B':0, 'I':1, 'E':2, 'S':3}
    x, y = [], []
    for line, label in zip(lines, labels):
        x_line, y_line = [], []
        for char, lab in zip(line, label):
            try:
                x_line.append(unigram_to_id[char])
            except KeyError:
                # Not into dictionary, substitute with UNK
                x_line.append(unigram_to_id['<UNK>'])
            y_line.append(encoding_labels[lab])
        x.append(x_line)
        y.append(y_line)
    y = [to_categorical(i, num_classes=N_CLASS) for i in y]
    y = np.asarray(y)
    # In order to feed them as Tensors, we need all of them to have equal length,
    # so we truncate long sentences and we pad short ones.
    print("\nPadding sequence to {} tokens".format(MAX_LENGTH))

    # When truncating, get rid of initial words and keep last part of the review. (longer sentences)
    # When padding, pad at the end of the sentence. (shorter sentences)
    x = pad_sequences(x, truncating='pre', padding='post', maxlen=MAX_LENGTH, value=0)
    y = pad_sequences(y, truncating='pre', padding='post', maxlen=MAX_LENGTH, value=0)

    return x, y


def create_bigrams(train_x_unigram, id_to_unigram, MAX_LENGHT):
    '''
    Create bigram training set from the corrisponding unigram
    :param train_x_unigram:
    :return: train_x_bigrams
    '''
    id_to_unigram = {v: k for k, v in unigram_to_id.items()}
    x = []
    # For training bigram set
    for train_line in train_x_unigram:
        x_line = []
        for i in range(0, train_line.shape[0]):
            if i != train_line.shape[0]-1:
                try:
                    x_line.append(bigram_to_id[str(id_to_unigram[train_line[i]] + id_to_unigram[train_line[i + 1]])])
                except KeyError: #Bigram not into bigram dictionary, should be count as padding
                    x_line.append(bigram_to_id['<PAD>'])
            else:
                x_line.append(bigram_to_id['<PAD>'])
        x.append(x_line)
    train_x_bigrams = pad_sequences(x, truncating='pre', padding='post', maxlen=MAX_LENGTH, value=0)

    return train_x_bigrams


def create_dictionaries(lines):
    '''
    Create unigram and bigram vocabolaries
    :param lines: a list of list of characters
    :return: Unigrams vocabulary, bigrams vocabulary, unigram to id vocabulary
    '''

    # Getting the input chars
    input_chars = []
    for line in lines:
        for i in range(0, len(line)):
            input_chars.append(line[i])

    # --- UNIGRAM DICTIONARY ---
    # The vocab is the set of characters
    vocab = list(set(input_chars))
    vocab = {k: v for (k, v) in zip(vocab, range(0, len(vocab)))}

    unigram_to_id = dict()
    unigram_to_id["<PAD>"] = 0  # zero is not casual!
    unigram_to_id["<START>"] = 1
    unigram_to_id["<UNK>"] = 2  # OOV are mapped as <UNK>
    unigram_to_id.update({k: v + len(unigram_to_id) for k, v in vocab.items()})

    id_to_unigram = {v: k for k, v in unigram_to_id.items()}

    # --- BIGRAM DICTIONARY ---
    bigrams = []
    for line in lines:
        bigrams += zip(*[line[i:] for i in range(2)])
    bigrams = list(set(bigrams))
    bigrams_vocab = {str(k[0] + k[1]): v for (k, v) in zip(bigrams, range(0, len(bigrams)))}

    bigram_to_id = dict()
    bigram_to_id["<PAD>"] = 0  # zero is not casual!
    bigram_to_id["<START>"] = 1
    bigram_to_id["<UNK>"] = 2  # OOV are mapped as <UNK>
    bigram_to_id.update({k: v + len(bigram_to_id) for k, v in bigrams_vocab.items()})

    id_to_bigram = {v: k for k, v in bigram_to_id.items()}

    return unigram_to_id, id_to_unigram, bigram_to_id, id_to_bigram


def create_embedded_input(n_grams, chinese_model, id_to_word):
    '''
    Create the embedded version of the input unigram/bigram using the pretrained chinese model
    :param n_grams: unigram or bigram list of list of char
    :param chinese_model: embedding chinese model
    :param index_to_id: vocabulary from index to id
    :return:
    '''
    embedded_output = []
    for padded_row in n_grams:
        embedded_row = []
        for char in padded_row:
            embedded_row.append(find_word_embedding(chinese_model, id_to_word[char]))
        embedded_output.append(np.asarray(embedded_row))
    return np.asarray(embedded_output)


if __name__ == '__main__':
    ''''
        Create datasets using Word2Vec chinese word embedding
    '''
    with open("data/cc.zh.300.vec") as f:
        for line in f:
            print(line[0])





    print("Loading chinese model....")
    # Loading the chinese word embedding model
    chinese_model = KeyedVectors.load_word2vec_format('data/cc.zh.300.vec')
    print("Model loaded.")

    # --- TRAINING DATASET ---
    ''' 
    paths = ['../icwb2-data/training/msr_training.utf8', '../icwb2-data/training/pku_training.utf8',
             '../icwb2-data/training/as_training_simplified.utf8', '../icwb2-data/training/cityu_training_simplified.utf8']
    print("Creating training input and label files...")
    training_lines, training_labels = create_input_label_file(paths)
    print("Building complete.")

    # Saving
    with open("data/training_lines_file.pkl", "wb") as f:
        pickle.dump(training_lines, f)

    with open("data/training_labels_file.pkl", "wb") as f:
        pickle.dump(training_labels, f)

    # --- DEVELOPMENT DATASET ---
    paths = ['../icwb2-data/gold/msr_test_gold.utf8', '../icwb2-data/gold/pku_test_gold.utf8',
             '../icwb2-data/gold/as_testing_gold_simplified.utf8', '../icwb2-data/gold/cityu_test_gold_simplified.utf8']
    print("Creating training input and label files...")
    gold_lines, gold_labels = create_input_label_file(paths)
    print("Building complete.")
    # Saving
    with open("data/dev_lines_file.pkl", "wb") as f:
        pickle.dump(gold_lines, f)

    with open("data/dev_labels_file.pkl", "wb") as f:
        pickle.dump(gold_labels, f)
    
    # Create dictionaries based on training set and save into pickle files
    print("Creating dictionaries....")
    unigram_to_id, id_to_unigram, bigram_to_id, id_to_bigram = create_dictionaries(training_lines)
    print("Dictionaries created.")

    # Saving to file
    with open("data/unigram_to_id.pkl", "wb") as f:
        pickle.dump(unigram_to_id, f)

    # Saving to file
    with open("data/bigram_to_id.pkl", "wb") as f:
        pickle.dump(bigram_to_id, f)

    # Saving to file
    with open("data/id_to_unigram.pkl", "wb") as f:
        pickle.dump(id_to_unigram, f)

    # Saving to file
    with open("data/id_to_bigram.pkl", "wb") as f:
        pickle.dump(id_to_bigram, f)

    '''
    with open("data/training_lines_file.pkl", "rb") as f:
        training_lines = pickle.load(f)

    with open("data/training_labels_file.pkl", "rb") as f:
        training_labels = pickle.load(f)

    with open("data/dev_lines_file.pkl", "rb") as f:
        gold_lines = pickle.load(f)

    with open("data/dev_labels_file.pkl", "rb") as f:
        gold_labels = pickle.load(f)

    with open("data/unigram_to_id.pkl", "rb") as f:
        unigram_to_id = pickle.load(f)

    with open("data/bigram_to_id.pkl", "rb") as f:
        bigram_to_id = pickle.load(f)

    with open("data/id_to_unigram.pkl", "rb") as f:
        id_to_unigram = pickle.load(f)

    with open("data/id_to_bigram.pkl", "rb") as f:
        id_to_bigram = pickle.load(f)

    # Find the median and using as padding size
    MAX_LENGTH = int(statistics.median([len(line) for line in training_lines]))
    N_CLASS = 4

    # Create the unigram and bigram for the TRAINING SET
    print("Creating training dataset...")
    train_x_unigram, train_y = create_unigrams_and_labels(training_lines, training_labels, N_CLASS, MAX_LENGTH)
    train_x_bigram = create_bigrams(train_x_unigram, id_to_unigram, MAX_LENGTH)
    # Transorm them with embedding model
    print("Transorming into embedding training dataset...")
    train_x_unigram = create_embedded_input(train_x_unigram, chinese_model, id_to_unigram)
    train_x_bigram = create_embedded_input(train_x_bigram, chinese_model, id_to_bigram)
    print("Done with training set.")

    # Saving to pickle files
    with open("data/train_x_unigrams.pkl", "wb") as f:
        pickle.dump(train_x_unigram, f)
    with open("data/train_x_bigram.pkl", "wb") as f:
        pickle.dump(train_x_bigram, f)
    with open("data/train_y.pkl", "wb") as f:
        pickle.dump(train_y, f)

    # Create the unigram and bigram for the DEVELOPMENT SET
    print("Creating development dataset...")
    dev_x_unigram, dev_y = create_unigrams_and_labels(gold_lines, gold_labels, N_CLASS, MAX_LENGTH)
    dev_x_bigram = create_bigrams(dev_x_unigram, id_to_unigram, MAX_LENGTH)
    # Transorm them with embedding model
    print("Transorming into embedding development dataset...")
    dev_x_unigram = create_embedded_input(dev_x_unigram, chinese_model, id_to_unigram)
    dev_x_bigram = create_embedded_input(dev_x_bigram, chinese_model, id_to_bigram)
    print("Done with development set.")

    # Saving to pickle files
    with open("data/dev_x_unigram.pkl", "wb") as f:
        pickle.dump(dev_x_unigram, f)
    with open("data/dev_x_bigram.pkl", "wb") as f:
        pickle.dump(dev_x_bigram, f)
    with open("data/dev_y.pkl", "wb") as f:
        pickle.dump(dev_y, f)