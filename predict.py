from argparse import ArgumentParser
from tensorflow.python.keras.models import model_from_json
from tensorflow.python.keras.preprocessing.sequence import pad_sequences
import pickle
import numpy as np


def parse_args():
    parser = ArgumentParser()
    parser.add_argument("input_path", help="The path of the input file")
    parser.add_argument("output_path", help="The path of the output file")
    parser.add_argument("resources_path", help="The path of the resources needed to load your model")

    return parser.parse_args()


def array_slicing(arr, size):
     output = []
     if arr.shape[1] <= size:
        return arr
     for i in range(0, arr.shape[1]//size + 1):
         if size*i+size <= arr.shape[1]:
             output.append(arr[0, size*i:size*i + size])
         else:
             output.append(arr[0, size*i:])
     return np.asarray(output)


def preprocess_input(input_path, resource_path, MAX_LENGHT):
    # --- LOAD INPUT FILE ---
    with open(input_path, encoding="utf-8") as f:
        lines_input = f.read().splitlines()
    lines = []
    for line in lines_input:
        line_foo = []
        for char in line:
            if char != ' ' and char != '\u3000':
                line_foo.append(char)
        lines.append(line_foo)

    # --- PREPROCESS INPUT ---
    # Load dictionaries
    with open(resource_path+"/unigram_to_id.pkl", "rb") as f:
        unigram_to_id = pickle.load(f)

    with open(resource_path+"/id_to_unigram.pkl", "rb") as f:
        id_to_unigram = pickle.load(f)

    with open(resource_path+"/bigram_to_id.pkl", "rb") as f:
        bigram_to_id = pickle.load(f)

    # Preparing unigrams array
    x_unigram = []
    for line in lines:
        x_line = []
        for char in line:
            try:
                x_line.append(unigram_to_id[char])
            except KeyError:
                # Not in dictionary
                x_line.append(unigram_to_id['<UNK>'])
        x_unigram.append(np.asarray(x_line))

    # Preparing bigrams array
    x_bigram = []
    # For training bigram set
    for line in x_unigram:
        x_line = []
        for i in range(0, len(line)):
            if i != len(line) - 1:
                try:
                    x_line.append(bigram_to_id[str(id_to_unigram[line[i]] + id_to_unigram[line[i + 1]])])
                except KeyError:
                    # Bigram not into bigram dictionary, should be count as unknown
                    x_line.append(bigram_to_id['<UNK>'])
            else:
                # End of line, so we pad
                x_line.append(bigram_to_id['<PAD>'])
        x_bigram.append(np.asarray(x_line))

    #x_unigram = pad_sequences(x_unigram, truncating='pre', padding='post', maxlen=MAX_LENGHT, value=0)
    #x_bigram = pad_sequences(x_bigram, truncating='pre', padding='post', maxlen=MAX_LENGHT, value=0)
    x_unigram = np.asarray(x_unigram)
    x_bigram = np.asarray(x_bigram)
    return x_unigram, x_bigram


def predict(input_path, output_path, resources_path):
    """
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the BIES format.
    
    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.
    
    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.

    :param input_path: the path of the input file to predict.
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    # --- LOAD MODEL ---
    # Load json file model
    json_file = open(resources_path+'model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # Load weights
    loaded_model.load_weights(resources_path+"model.h5")
    print("Loaded model from disk")

    # --- PREPROCESS INPUT ---
    MAX_LENGHT = 24 # padding size
    x_unigram, x_bigram = preprocess_input(input_path, resources_path, MAX_LENGHT)

    # -- PREDICT AND WRITING TO OUTPUT FILE ---
    encoding_labels = {0:'B', 1:'I', 2:'E', 3:'S'}
    with open(output_path, "a") as f:
        for x_unigram_line, x_bigram_line in zip(x_unigram, x_bigram):
            # Reshaping
            x_unigram_line = x_unigram_line.reshape((1, x_unigram_line.shape[0]))
            x_bigram_line = x_bigram_line.reshape((1, x_bigram_line.shape[0]))

            # Here I split the line into chunk of size MAX LENGHT and I predict the labels for each
            for unigram_line, bigram_line in zip(array_slicing(x_unigram_line, MAX_LENGHT),
                                                 array_slicing(x_bigram_line, MAX_LENGHT)):
                try:
                    # Reshape again
                    unigram_line = unigram_line.reshape((1, unigram_line.shape[0]))
                    bigram_line = bigram_line.reshape((1, bigram_line.shape[0]))

                    # Pad if the len is less than padding size
                    if unigram_line.shape[1] < MAX_LENGHT:
                        unigram_line = pad_sequences(unigram_line, truncating='pre', padding='post', maxlen=MAX_LENGHT, value=0)
                        bigram_line = pad_sequences(bigram_line, truncating='pre', padding='post', maxlen=MAX_LENGHT, value=0)

                    # Predict and transform into BIES labels
                    predictions = loaded_model.predict([unigram_line, bigram_line])
                    predictions = np.squeeze(predictions)
                    predictions = [encoding_labels[np.argmax(prediction)] for prediction in predictions]

                    # Write to output file
                    for char, prediction in zip(unigram_line[0], predictions):
                        if char != 0:  # Zero is the value of padding char
                            f.write(prediction)
                except Exception as e:
                    print(e)
                    print(unigram_line.shape)
            f.write("\n")
    print("DONE")


if __name__ == '__main__':
    args = parse_args()
    predict(args.input_path, args.output_path, args.resources_path)
    '''
    input, output, resources = "../icwb2-data/gold/msr_test_gold.utf8", "resource/msr_test_gold_predicted.utf8", "resource/"
    predict(input, output, resources)
    '''