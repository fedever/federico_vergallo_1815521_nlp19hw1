import tensorflow as tf
import pickle
from keras.models import Model, Input
from keras.layers import LSTM, Embedding, Dense, TimeDistributed, Dropout, concatenate, merge
from keras.optimizers import SGD, Adam
from keras.callbacks import TensorBoard, ModelCheckpoint, EarlyStopping
import keras.backend as K
from keras.regularizers import l2


# Open unigram vocab and bigram vocab
with open("resources/dictionary.pkl", "rb") as f:
    unigram_to_id = pickle.load(f)

with open("resources/dictionary_bigrams.pkl", "rb") as f:
    bigram_to_id = pickle.load(f)


# DEFINE SOME COSTANTS
MAX_LENGTH = 24
VOCAB_SIZE_UNIGRAM = len(unigram_to_id.keys())
VOCAB_SIZE_BIGRAM = len(bigram_to_id.keys())
EMBEDDING_SIZE_UNIGRAM = 64
EMBEDDING_SIZE_BIGRAM = 16
HIDDEN_SIZE = 256
N_CLASS = 4


def create_keras_model():
    print("Creating KERAS model")
    # Inputs for both Unigrams and Bigrams
    input_unigram = Input(shape=(MAX_LENGTH,), name='Unigram_input')
    input_bigram = Input(shape=(MAX_LENGTH,), name='Bigram_input')

    # Embedding each input
    unigram_embedding = Embedding(VOCAB_SIZE_UNIGRAM, EMBEDDING_SIZE_UNIGRAM, mask_zero=True)(input_unigram)
    bigram_embedding = Embedding(VOCAB_SIZE_BIGRAM, EMBEDDING_SIZE_BIGRAM, mask_zero=True)(input_bigram)

    # Concatenate embedded unigram and bigram
    input = concatenate([unigram_embedding, bigram_embedding])

    # Forward LSTM layer
    model_bwd = LSTM(units=HIDDEN_SIZE, recurrent_dropout=0.2, return_sequences=True, go_backwards=True,)(input)
    # Backward LSTM Layer
    model_fwd = LSTM(units=HIDDEN_SIZE, recurrent_dropout=0.2, return_sequences=True)(input)

    out = concatenate([model_bwd, model_fwd])

    # Output layer for each char
    out = TimeDistributed(Dense(N_CLASS, activation="softmax"))(out)  # softmax output layer

    model = Model(inputs=[input_unigram, input_bigram], output=[out])
    # Optimizer
    optimizer = Adam(lr=0.04, decay=1e-6)

    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['acc'])

    return model


def add_summary(writer, name, value, global_step):
    summary = tf.Summary(value=[tf.Summary.Value(tag=name, simple_value=value)])
    writer.add_summary(summary, global_step=global_step)



if __name__ == '__main__':
    epochs = 100
    batch_size = 128
    # Training set restoring
    with open("resources/train_x_unigram.pkl", "rb") as f:
        train_x_unigram = pickle.load(f)
    with open("resources/train_x_bigram.pkl", "rb") as f:
        train_x_bigram = pickle.load(f)
    with open("resources/train_y.pkl", "rb") as f:
        train_y = pickle.load(f)
    # Development set restoring
    with open("resources/dev_x_unigram.pkl", "rb") as f:
        dev_x_unigram = pickle.load(f)
    with open("resources/dev_x_bigram.pkl", "rb") as f:
        dev_x_bigram = pickle.load(f)
    with open("resources/dev_y.pkl", "rb") as f:
        dev_y = pickle.load(f)

    print("Training set shape:", train_x_unigram.shape)
    print("Label training set shape:", train_y.shape)

    print("Development set shape:", dev_x_unigram.shape)
    print("Label development set shape:", dev_y.shape)

    model = create_keras_model()

    # Let's print a summary of the model
    model.summary()

    try:
        cbk = TensorBoard("resources")
        mc = ModelCheckpoint('resources/weights{epoch:08d}.h5', 
                                     save_weights_only=True, period=1)
        es = EarlyStopping(monitor='val_loss',
                              min_delta=0,
                              patience=5,
                              verbose=0, mode='auto')
        print("\nStarting training...")
        model.fit([train_x_unigram, train_x_bigram], train_y, epochs=epochs, batch_size=batch_size,
                  shuffle=True, validation_data=([dev_x_unigram, dev_x_bigram], dev_y), callbacks=[cbk, mc, es])
        print("Training complete.\n")
    except KeyboardInterrupt:
        print("\nEvaluating test...")
        loss_acc = model.evaluate([dev_x_unigram, dev_x_bigram], dev_y, verbose=0)
        print("Test data: loss = %0.6f  accuracy = %0.2f%% precision = %0.2f%%" % (loss_acc[0], loss_acc[1] * 100))

    # serialize model to JSON
    model_json = model.to_json()
    with open("resources/model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("resources/model.h5")
    print("Saved model to disk")
    ''' 
    # later...

    # load json and create model
    json_file = open('resources/model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("resources/model.h5")
    print("Loaded model from disk")'''